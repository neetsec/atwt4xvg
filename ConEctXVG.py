from configXVG import vergeData as xvgD
import vergerpc as xvgrpc
import sys

class ConEctXVG(object):
    '''Manage connections for the vergeDaemon'''

    def __init__(self,xvgData = xvgD):
        '''
        Constructor 

        :type xvgData: dict
        :param xvgData: data for connection
        '''
        self.xvgData = xvgData

    def db_connect(self):
        '''
        connects to the verge daemon
        
        :returns: conector
        '''
        #if local is True then connect to local
        if self.xvgData['local'] == True:
            conn = xvgrpc.connect_to_local()
        #create remote connection with xvgData
        else: 
            try:
                conn  = xvgrpc.connect_to_remote(**self.xvgData)
            except:
                sys.exit("it's not posible to connect to VERGEd, daemons!")
        #return de connector (working) for VERGEd
        return conn


if __name__ == "__main__":
    pass

