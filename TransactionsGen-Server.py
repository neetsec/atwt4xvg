#!/usr/bin/env python3
#The Verge Lotto ticket generator
import datetime
from configXVG import vergeTransactionsData as vlD
from ConEctXVG import ConEctXVG
from ConEct import ConEct
import hashlib
#import needed for the rpc json server
from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple

from jsonrpc import JSONRPCResponseManager, dispatcher

@dispatcher.add_method
def transactionGen():
    '''
    this function returns a new unique address
    a sha224 cut by 128 bits for url in the
    Anon Token Web Transactions and the balance in json format
    '''
    #container dictionary for return
    generatedData = {}
    #we create object for sha224 and the url hash generation
    m=hashlib.sha224()
    #price for one transaction in XVG
    #trasanctionPrice=vlD['trasanctionPrice']

    #create a connection object to the verge daemon
    ceXVG = ConEctXVG()
    conn = ceXVG.db_connect()

    #generate a new address for transaction deposit
    generatedData['address'] = conn.getnewaddress(account=vlD['account'])
    #verify if address exist in the db public.txrep, and if not---

    #link with the sha224 por that txrep.
    #we sha224 the uniqueAccount of verge
    generatedData['urlhash']=generatedData['address'].encode('utf-8')
    m.update(generatedData['urlhash'])
    #we get 128 bits from the sha224 for the urlhash
    generatedData['urlhash']=m.hexdigest()[:16]
    
    #get address balance
    generatedData['balance'] = conn.getreceivedbyaddress(generatedData['address']) 
    #probably change type for sending the data str wont brake json
    generatedData['balance'] = str(generatedData['balance'])
    return generatedData

@dispatcher.add_method
def addressBalance(*args): 
    '''
    function to get Balance by address or addresses
    as argument
    '''
    lista=[]
    #create a connection object to the verge daemon
    ceXVG = ConEctXVG()
    conn = ceXVG.db_connect()

    #if no address:
    if len(args) == 0:
        lista  = 'no address'
    #if its one address
    elif len(args) == 1:
        lista = conn.getreceivedbyaddress(args)
    #if its a list of addresses
    else:
        for arg in args:
            lista.append(conn.getreceivedbyaddress(arg))
    print(lista) 
    return lista

@Request.application
def application(request):
    # Dispatcher is dictionary {<method_name>: callable}

    response = JSONRPCResponseManager.handle(
        request.data, dispatcher)
    return Response(response.json, mimetype='application/json')


if __name__ == '__main__':
    run_simple('0.0.0.0', 54321, application)

